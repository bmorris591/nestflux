package uk.co.borismorris.nestflux.dao

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import uk.co.borismorris.nestflux.domain.Structure
import uk.co.borismorris.nestflux.domain.StructureId
import uk.co.borismorris.nestflux.google.DeviceAccessApi
import java.util.concurrent.atomic.AtomicReference

interface StructureDao {
    suspend fun structure(structureId: StructureId): Structure

    suspend fun structures(): Flow<Structure>

    fun refresh()
}

class DeviceAccessApiStructureDao(private val deviceAccessApi: DeviceAccessApi) : StructureDao {
    private val structures = AtomicReference<Deferred<Map<StructureId, Structure>>>()

    override suspend fun structure(structureId: StructureId) = getStructures().getValue(structureId)

    override suspend fun structures() = getStructures().values.asFlow()

    override fun refresh() {
        structures.set(null)
    }

    private suspend fun getStructures() = structures.updateAndGet {
        it ?: GlobalScope.async {
            deviceAccessApi.structures().map { it.id to it }.toMap()
        }
    }.await()
}
