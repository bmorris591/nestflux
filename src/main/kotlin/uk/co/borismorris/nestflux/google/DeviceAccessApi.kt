package uk.co.borismorris.nestflux.google

import com.google.api.client.http.apache.v2.ApacheHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.smartdevicemanagement.v1.SmartDeviceManagement
import com.google.api.services.smartdevicemanagement.v1.SmartDeviceManagement.Enterprises
import com.google.api.services.smartdevicemanagement.v1.SmartDeviceManagementRequest
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.UserCredentials
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import uk.co.borismorris.nestflux.config.props.NestProps
import uk.co.borismorris.nestflux.domain.Device
import uk.co.borismorris.nestflux.domain.Room
import uk.co.borismorris.nestflux.domain.Structure
import uk.co.borismorris.nestflux.domain.StructureId
import uk.co.borismorris.nestflux.domain.toDevice
import uk.co.borismorris.nestflux.domain.toRoom
import uk.co.borismorris.nestflux.domain.toSmdObjectId
import uk.co.borismorris.nestflux.domain.toStructure

interface DeviceAccessApi {
    fun structures(): Flow<Structure>

    fun rooms(structure: StructureId): Flow<Room>

    fun devices(): Flow<Device>
}

class GoogleSdmDeviceAccess(private val nestProps: NestProps) : DeviceAccessApi {
    val creds = UserCredentials.newBuilder()
        .setClientId(nestProps.sdmClient.clientId)
        .setClientSecret(nestProps.sdmClient.clientSecret)
        .setRefreshToken(nestProps.sdmClient.refreshToken)
        .build()

    val parent = "enterprises/${nestProps.projectId}"
    val smartDeviceManagement = SmartDeviceManagement.Builder(ApacheHttpTransport(), JacksonFactory.getDefaultInstance(), HttpCredentialsAdapter(creds))
        .setApplicationName("nestflux")
        .build()

    override fun structures() = flowItems(
        { enterprises, nextPage -> enterprises.structures().list(parent).setPageToken(nextPage) },
        { it.nextPageToken },
        { it.structures }
    ).map { it.toStructure() }

    override fun rooms(structure: StructureId) = flowItems(
        { enterprises, nextPage -> enterprises.structures().rooms().list(structure.toSmdObjectId(nestProps.projectId).id).setPageToken(nextPage) },
        { it.nextPageToken },
        { it.rooms }
    ).map { it.toRoom() }

    override fun devices() = flowItems(
        { enterprises, nextPage -> enterprises.devices().list(parent).setPageToken(nextPage) },
        { it.nextPageToken },
        { it.devices }
    ).map { it.toDevice() }

    private fun <RESPONSE, ITEM> flowItems(request: (Enterprises, String?) -> SmartDeviceManagementRequest<RESPONSE>, nextPage: (RESPONSE) -> String?, result: (RESPONSE) -> List<ITEM>): Flow<ITEM> = flow {
        var nextPageToken: String? = null
        do {
            val response = request(smartDeviceManagement.enterprises(), nextPageToken)
                .execute()
            emitAll(result(response).asFlow())
            nextPageToken = nextPage(response)
        } while (nextPageToken != null)
    }
}
