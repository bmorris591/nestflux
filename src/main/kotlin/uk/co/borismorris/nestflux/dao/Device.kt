package uk.co.borismorris.nestflux.dao

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import uk.co.borismorris.nestflux.domain.Device
import uk.co.borismorris.nestflux.domain.DeviceId
import uk.co.borismorris.nestflux.google.DeviceAccessApi
import java.util.concurrent.atomic.AtomicReference

interface DeviceDao {
    suspend fun device(deviceId: DeviceId): Device

    suspend fun devices(): Flow<Device>

    fun refresh()
}

class DeviceAccessApiDeviceDao(private val deviceAccessApi: DeviceAccessApi) : DeviceDao {
    private val devices = AtomicReference<Deferred<Map<DeviceId, Device>>>()

    override suspend fun device(deviceId: DeviceId) = getDevices().getValue(deviceId)

    override suspend fun devices() = getDevices().values.asFlow()

    override fun refresh() {
        devices.set(null)
    }

    private suspend fun getDevices() = devices.updateAndGet {
        it ?: GlobalScope.async {
            deviceAccessApi.devices().map { it.id to it }.toMap()
        }
    }.await()
}
