package uk.co.borismorris.nestflux.dao

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import uk.co.borismorris.nestflux.domain.Room
import uk.co.borismorris.nestflux.domain.RoomId
import uk.co.borismorris.nestflux.domain.StructureId
import uk.co.borismorris.nestflux.google.DeviceAccessApi

interface RoomDao {
    suspend fun room(structureId: StructureId, roomId: RoomId): Room

    suspend fun rooms(structureId: StructureId): Flow<Room>
}

class DeviceAccessApiRoomDao(private val deviceAccessApi: DeviceAccessApi) : RoomDao {
    private val rooms: LoadingCache<StructureId, Deferred<Map<RoomId, Room>>> = CacheBuilder.newBuilder()
        .build(
            CacheLoader.from { structureId ->
                GlobalScope.async {
                    deviceAccessApi.rooms(structureId!!).map { it.id to it }.toMap()
                }
            }
        )

    override suspend fun room(structureId: StructureId, roomId: RoomId) = rooms[structureId].await().getValue(roomId)

    override suspend fun rooms(structureId: StructureId) = rooms[structureId].await().values.asFlow()

    fun refresh() {
        rooms.invalidateAll()
    }
}
