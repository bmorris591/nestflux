package uk.co.borismorris.nestflux.config.props

class NestProps {
    lateinit var projectId: String
    val sdmClient = SdmClientProps()
    val pubSubClient = PubSubClientProps()
}

class SdmClientProps {
    lateinit var refreshToken: String
    lateinit var clientId: String
    lateinit var clientSecret: String

    override fun toString() = "SdmClientProps(refreshToken='XXXX', clientId='$clientId', clientSecret='XXXX')"
}

class PubSubClientProps {
    lateinit var privateKeyId: String
    lateinit var privateKeyPkcs8: String
    lateinit var clientId: String
    lateinit var clientEmail: String

    lateinit var projectId: String
    lateinit var subscription: String

    override fun toString(): String {
        return "PubSubClientProps(privateKeyId='$privateKeyId', privateKeyPkcs8='XXXX', clientId='$clientId', clientEmail='$clientEmail', projectId='$projectId', subscription='$subscription')"
    }
}
