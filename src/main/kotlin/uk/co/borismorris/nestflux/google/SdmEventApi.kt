package uk.co.borismorris.nestflux.google

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.node.ObjectNode
import com.google.api.core.ApiService
import com.google.api.gax.core.FixedCredentialsProvider
import com.google.auth.oauth2.ServiceAccountCredentials
import com.google.cloud.pubsub.v1.AckReplyConsumer
import com.google.cloud.pubsub.v1.MessageReceiver
import com.google.cloud.pubsub.v1.Subscriber
import com.google.common.util.concurrent.MoreExecutors
import com.google.protobuf.ByteString
import com.google.protobuf.Timestamp
import com.google.pubsub.v1.ProjectSubscriptionName
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.UNLIMITED
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.runBlocking
import mu.KLogging
import uk.co.borismorris.nestflux.config.props.NestProps
import uk.co.borismorris.nestflux.domain.DeviceTrait
import uk.co.borismorris.nestflux.domain.RoomTrait
import uk.co.borismorris.nestflux.domain.SdmObjectId
import uk.co.borismorris.nestflux.domain.StructureTrait
import uk.co.borismorris.nestflux.domain.Trait
import uk.co.borismorris.nestflux.domain.UnknownTrait
import uk.co.borismorris.nestflux.domain.parseTraits
import java.time.Instant
import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

interface SdmEventApi {
    fun subscribe(): ReceiveChannel<PubSubEvent>
}

interface AckabableEvent {
    fun ack()
    fun nack()
}

interface PubSubEvent : AckabableEvent {
    val eventId: String
    val timestamp: Instant
    val attributes: Map<String, String>
    val sdmEvent: SdmEvent
}

@JsonSubTypes(
    JsonSubTypes.Type(value = SdmRelationUpdateEvent::class, name = "relationUpdate"),
    JsonSubTypes.Type(value = SdmResourceUpdateEvent::class, name = "resourceUpdate")
)

sealed class SdmEvent {
    @JsonProperty("eventId")
    lateinit var eventId: String

    @JsonProperty("userId")
    lateinit var userId: String

    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSVV")
    lateinit var timestamp: Instant
}

class SdmRelationUpdateEvent : SdmEvent() {
    @JsonProperty("relationUpdate")
    lateinit var relationUpdate: SdmRelationUpdate
}

class SdmResourceUpdateEvent : SdmEvent() {
    @JsonProperty("resourceUpdate")
    lateinit var resourceUpdate: SdmResourceUpdate
}

class SdmRelationUpdate {
    @JsonProperty("type")
    lateinit var type: Type

    @JsonProperty("subject")
    var subject: SdmObjectId = SdmObjectId("")

    @JsonProperty("object")
    var `object`: SdmObjectId = SdmObjectId("")
}

enum class Type {
    CREATED,
    DELETED,
    UPDATED;
}

class SdmResourceUpdate {
    @JsonProperty("name")
    var name: SdmObjectId = SdmObjectId("")

    @JsonProperty("traits")
    lateinit var traits: Map<String, Trait>
}

class GooglePubSubSdmEvents(nestProps: NestProps, mapper: ObjectMapper) : SdmEventApi {

    companion object : KLogging()

    private val serviceAccount = with(nestProps.pubSubClient) {
        ServiceAccountCredentials.fromPkcs8(
            clientId,
            clientEmail,
            privateKeyPkcs8,
            privateKeyId,
            mutableSetOf()
        )
    }
    private val credentials = FixedCredentialsProvider.create(serviceAccount)
    private val subscription = with(nestProps.pubSubClient) {
        ProjectSubscriptionName.of(projectId, subscription)
    }
    private val mapper = mapper.copy().enableSdm()

    override fun subscribe(): ReceiveChannel<PubSubEvent> {
        val channel = Channel<PubSubEvent>(UNLIMITED)
        val messageReceiver = MessageReceiver { message, ack ->
            runBlocking {
                channel.send(GooglePubSubEvent(message.messageId, message.publishTime.toInstant(), message.attributesMap, message.data, ack))
            }
        }

        val subscriber = Subscriber.newBuilder(subscription, messageReceiver).setCredentialsProvider(credentials).build()
        subscriber.addListener(
            object : ApiService.Listener() {
                override fun starting() {
                    logger.info("Subscription listener is starting")
                }

                override fun running() {
                    logger.info("Subscription listener has started")
                }

                override fun stopping(from: ApiService.State?) {
                    logger.info("Subscription listener is stopping, was in state {}", from)
                }

                override fun failed(from: ApiService.State?, failure: Throwable?) {
                    logger.error("Subscription listener has failed, was in state {}", from, failure)
                }
            },
            MoreExecutors.directExecutor()
        )

        channel.invokeOnClose { subscriber.stopAsync() }

        subscriber.startAsync()

        return channel
    }

    private fun Timestamp.toInstant() = Instant.ofEpochSecond(seconds, nanos.toLong())

    inner class GooglePubSubEvent(
        override val eventId: String,
        override val timestamp: Instant,
        override val attributes: Map<String, String>,
        private val data: ByteString,
        private val ack: AckReplyConsumer
    ) : PubSubEvent {
        override val sdmEvent: SdmEvent by lazy { parseSdmEvent(data) }

        override fun ack() = ack.ack()
        override fun nack() = ack.nack()

        private fun parseSdmEvent(data: ByteString) = data.newInput().use { mapper.readValue(it, SdmEvent::class.java) }

        override fun toString() = "GooglePubSubEvent(eventId='$eventId', timestamp=$timestamp, attributes=$attributes, data=$data, ack=$ack)"
    }
}

val sdmModule = SimpleModule("sdm-module").run {
    addDeserializer(SdmEvent::class.java, PresentPropertyPolymorphicDeserializer(SdmEvent::class))
    addDeserializer(Trait::class.java, TraitDeserializer())
}

fun ObjectMapper.enableSdm() = this.apply {
    registerModule(sdmModule)
    enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS)
    enable(DeserializationFeature.USE_BIG_INTEGER_FOR_INTS)
}

class PresentPropertyPolymorphicDeserializer<T : Any>(vc: KClass<T>) : StdDeserializer<T>(vc.java) {
    private val propertyNameToType: Map<String, KClass<*>> = vc.findAnnotation<JsonSubTypes>()!!.value.associate { it.name to it.value }

    @Suppress("UNCHECKED_CAST")
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): T {
        val objectNode = p.codec.readTree<ObjectNode>(p)
        return propertyNameToType.asSequence()
            .find { objectNode.has(it.key) }
            ?.let { p.codec.treeToValue(objectNode, it.value.java) as T }
            ?: throw IllegalArgumentException("could not infer to which class to deserialize $objectNode")
    }
}

@Suppress("UNUSED_VARIABLE")
class TraitDeserializer : StdDeserializer<Trait>(Trait::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Trait {
        val key = ctxt.getParser().currentName
        val traits: Map<String, Any> = p.codec.readValue(p, ctxt.typeFactory.constructMapLikeType(Map::class.java, String::class.java, Object::class.java))
        val input = mapOf(key to traits)

        return parseTraits(input).map { it.value }.firstOrNull() ?: UnknownTrait(key, traits)
    }

    private fun parseTraits(input: Map<String, Map<String, Any>>) =
        parseTraits<StructureTrait>(input).asSequence() + parseTraits<RoomTrait>(input).asSequence() + parseTraits<DeviceTrait>(input).asSequence()
}
