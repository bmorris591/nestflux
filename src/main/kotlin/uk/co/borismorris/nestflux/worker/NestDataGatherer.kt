package uk.co.borismorris.nestflux.worker

import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ticker
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.joinAll
import mu.KLogging
import org.springframework.context.event.ApplicationEventMulticaster
import uk.co.borismorris.nestflux.dao.DeviceDao
import uk.co.borismorris.nestflux.event.RelationUpdateEvent
import uk.co.borismorris.nestflux.google.SdmEvent
import uk.co.borismorris.nestflux.google.SdmEventApi
import uk.co.borismorris.nestflux.google.SdmRelationUpdateEvent
import uk.co.borismorris.nestflux.google.SdmResourceUpdateEvent
import uk.co.borismorris.reactiveflux.InfluxClient
import uk.co.borismorris.reactiveflux.convertAndSendToInflux
import java.time.Duration

@OptIn(ObsoleteCoroutinesApi::class)
class GatherNestData(
    private val sdmEventApi: SdmEventApi,
    private val influxClient: InfluxClient,
    private val deviceDao: DeviceDao,
    private val eventMulticaster: ApplicationEventMulticaster
) {
    companion object : KLogging()

    suspend fun run() = coroutineScope {
        val dataChannel = Channel<Any>(1024)
        val subscriber = async {
            logger.info("Start pulling events")
            sdmEventApi.subscribe().consumeAsFlow().collect { event ->
                try {
                    processEvent(event.sdmEvent)
                    event.ack()
                } catch (e: Exception) {
                    event.nack()
                    throw e
                }
            }
        }
        val poller = async {
            logger.info("Start polling devices")
            ticker(Duration.ofMinutes(30).toMillis(), 0L).consumeAsFlow()
                .onEach { deviceDao.refresh() }
                .flatMapConcat { deviceDao.devices() }
                .collect {
                    dataChannel.send(it)
                }
        }

        dataChannel.consumeAsFlow()
            .convertAndSendToInflux(influxClient)
            .onCompletion {
                subscriber.cancel()
                poller.cancel()
            }
            .collect { logger.trace("Sent item to Influx {}", it) }

        joinAll(subscriber, poller)
    }

    private suspend fun processEvent(event: SdmEvent) {
        if (event is SdmRelationUpdateEvent)
            processEvent(event)
        else if (event is SdmResourceUpdateEvent) {
            processEvent(event)
        }
    }

    private fun processEvent(event: SdmRelationUpdateEvent) {
        logger.info("Got a relation update event {}. Refresh DAOs.", event)
        eventMulticaster.multicastEvent(RelationUpdateEvent(event))
    }

    private suspend fun processEvent(event: SdmResourceUpdateEvent) {
        flowOf(event).convertAndSendToInflux(influxClient)
            .collect { logger.trace("Sent event to Influx {}", it) }
    }
}
