package uk.co.borismorris.nestflux.domain

import com.google.api.services.smartdevicemanagement.v1.model.GoogleHomeEnterpriseSdmV1Room

private val ROOM_ID_PATTERN = "enterprises/(?<enterpriseId>[^/]++)/structures/(?<structureId>[^/]++)/rooms/(?<roomId>.*+)".toRegex()

data class Room(
    override val id: RoomId,
    override val objectId: SdmObjectId,
    val structureId: StructureId,
    override val traits: Map<String, RoomTrait>
) : SdmObject {
    override val customName: String by lazy { (traits.values.find { it is RoomInfoTrait } as RoomInfoTrait).customName }
}

inline class RoomId(override val id: String) : Id

fun GoogleHomeEnterpriseSdmV1Room.toRoom() = SdmObjectId(name).let {
    val (structureId, roomId) = it.getRoomId()
    Room(roomId, it, structureId, parseTraits(traits))
}

fun SdmObjectId.getRoomId(): Pair<StructureId, RoomId> {
    val match = ROOM_ID_PATTERN.matchEntire(id)
        ?: throw IllegalArgumentException("Invalid room name format")

    val structureId = match.groups["structureId"]?.value
        ?: throw IllegalArgumentException("Invalid room name structure format")
    val roomId = match.groups["roomId"]?.value ?: throw IllegalArgumentException("Invalid room id format")

    return StructureId(structureId) to RoomId(roomId)
}

sealed class RoomTrait : Trait()

@TraitName("sdm.structures.traits.RoomInfo")
data class RoomInfoTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : RoomTrait() {
    val customName: String by data
}
