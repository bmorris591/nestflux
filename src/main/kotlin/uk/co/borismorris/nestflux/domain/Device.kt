package uk.co.borismorris.nestflux.domain

import com.google.api.services.smartdevicemanagement.v1.model.GoogleHomeEnterpriseSdmV1Device
import java.math.BigDecimal
import java.time.Instant

private val DEVICE_ID_PATTERN = "enterprises/(?<enterpriseId>[^/]++)/devices/(?<deviceId>.*+)".toRegex()

data class Device(
    override val id: DeviceId,
    override val objectId: SdmObjectId,
    val structureId: StructureId,
    val roomId: RoomId,
    override val traits: Map<String, DeviceTrait>
) : SdmObject {
    override val customName: String by lazy { (traits.values.find { it is DeviceInfoTrait } as DeviceInfoTrait).customName }
}

inline class DeviceId(override val id: String) : Id

fun GoogleHomeEnterpriseSdmV1Device.toDevice() = SdmObjectId(name).let {
    val deviceId = it.getDeviceId()
    val (structureId, roomId) = getLocationId()
    Device(deviceId, SdmObjectId(name), structureId, roomId, parseTraits(traits))
}

fun SdmObjectId.getDeviceId(): DeviceId {
    val match = DEVICE_ID_PATTERN.matchEntire(id) ?: throw IllegalArgumentException("Invalid device id format")
    val id = match.groups["deviceId"]?.value ?: throw IllegalArgumentException("Invalid device id format")
    return DeviceId(id)
}

private fun GoogleHomeEnterpriseSdmV1Device.getLocationId() = SdmObjectId(assignee).getRoomId()

sealed class DeviceTrait : Trait()

@TraitName("sdm.devices.traits.Connectivity")
data class ConnectivityTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val status: String by data
}

@TraitName("sdm.devices.traits.Fan")
data class FanTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val timerMode: String by data
    val timerTimeout: Instant by data
}

@TraitName("sdm.devices.traits.Humidity")
data class HumidityTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val ambientHumidityPercent: BigDecimal by data
}

@TraitName("sdm.devices.traits.Info")
data class DeviceInfoTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val customName: String by data
}

@TraitName("sdm.devices.traits.Settings")
data class SettingsTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val temperatureScale: String by data
}

@TraitName("sdm.devices.traits.Temperature")
data class TemperatureTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val ambientTemperatureCelsius: BigDecimal by data
}

@TraitName("sdm.devices.traits.ThermostatEco")
data class ThermostatEcoTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val availableModes: Array<String> by data
    val mode: String by data
    val heatCelsius: BigDecimal by data
    val coolCelsius: BigDecimal by data
}

@TraitName("sdm.devices.traits.ThermostatHvac")
data class ThermostatHvacTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val status: String by data
}

@TraitName("sdm.devices.traits.ThermostatMode")
data class ThermostatModeTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val availableModes: Array<String> by data
    val mode: String by data
}

@TraitName("sdm.devices.traits.ThermostatTemperatureSetpoint")
data class ThermostatTemperatureSetpointTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : DeviceTrait() {
    val heatCelsius: BigDecimal by data
    val coolCelsius: BigDecimal by data
}
