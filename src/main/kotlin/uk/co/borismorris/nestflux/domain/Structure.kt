package uk.co.borismorris.nestflux.domain

import com.google.api.services.smartdevicemanagement.v1.model.GoogleHomeEnterpriseSdmV1Structure

private val STRUCTURE_ID_PATTERN = "enterprises/(?<enterpriseId>[^/]++)/structures/(?<structureId>.*+)".toRegex()

data class Structure(
    override val id: StructureId,
    override val objectId: SdmObjectId,
    override val traits: Map<String, StructureTrait>
) : SdmObject {
    override val customName: String by lazy { (traits.values.find { it is StructureInfoTrait } as StructureInfoTrait).customName }
}

inline class StructureId(override val id: String) : Id

fun StructureId.toSmdObjectId(enterpriseId: String) = SdmObjectId("enterprises/$enterpriseId/structures/$id")

fun GoogleHomeEnterpriseSdmV1Structure.toStructure() = SdmObjectId(name).let { Structure(it.getStructureId(), it, parseTraits(traits)) }

fun SdmObjectId.getStructureId(): StructureId {
    val match = STRUCTURE_ID_PATTERN.matchEntire(id) ?: throw IllegalArgumentException("Invalid structure id format")
    val id = match.groups["structureId"]?.value ?: throw IllegalArgumentException("Invalid structure id format")
    return StructureId(id)
}

sealed class StructureTrait : Trait()

@TraitName("sdm.structures.traits.Info")
data class StructureInfoTrait @TraitConstructor constructor(private val data: Map<String, Any?>) : StructureTrait() {
    val customName: String by data
}
