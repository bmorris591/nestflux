package uk.co.borismorris.nestflux.influx.webclient

import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flowOf
import uk.co.borismorris.nestflux.dao.DeviceDao
import uk.co.borismorris.nestflux.dao.RoomDao
import uk.co.borismorris.nestflux.dao.StructureDao
import uk.co.borismorris.nestflux.domain.ConnectivityTrait
import uk.co.borismorris.nestflux.domain.Device
import uk.co.borismorris.nestflux.domain.DeviceTrait
import uk.co.borismorris.nestflux.domain.HumidityTrait
import uk.co.borismorris.nestflux.domain.Room
import uk.co.borismorris.nestflux.domain.Structure
import uk.co.borismorris.nestflux.domain.TemperatureTrait
import uk.co.borismorris.nestflux.domain.ThermostatEcoTrait
import uk.co.borismorris.nestflux.domain.ThermostatHvacTrait
import uk.co.borismorris.nestflux.domain.ThermostatModeTrait
import uk.co.borismorris.nestflux.domain.ThermostatTemperatureSetpointTrait
import uk.co.borismorris.nestflux.domain.Trait
import uk.co.borismorris.nestflux.domain.getDeviceId
import uk.co.borismorris.nestflux.google.SdmResourceUpdateEvent
import uk.co.borismorris.reactiveflux.InfluxMeasurement
import uk.co.borismorris.reactiveflux.InfluxMeasurementBuilder
import uk.co.borismorris.reactiveflux.InfluxMeasurementConverter
import uk.co.borismorris.reactiveflux.measurement
import java.time.Instant

class DevicePointConverter(private val structureDao: StructureDao, private val roomDao: RoomDao) : InfluxMeasurementConverter {
    override suspend fun convert(source: Any) = if (source is Device)
        flowOf(convert(source)).filter { it.fields.isNotEmpty() }
    else
        throw IllegalArgumentException("Invalid type $source. Must call canConvert.")

    override fun canConvert(source: Any): Boolean = source is Device

    private suspend fun convert(source: Device): InfluxMeasurement {
        val location = source.location(structureDao, roomDao)

        return measurement("temperature") {
            time(Instant.now())
            addDeviceTags(source, location)
            addFields(source.traits.values)
        }
    }
}

class SdmEventPointConverter(
    private val deviceDao: DeviceDao,
    private val structureDao: StructureDao,
    private val roomDao: RoomDao
) : InfluxMeasurementConverter {
    override suspend fun convert(source: Any) = if (source is SdmResourceUpdateEvent)
        flowOf(convert(source)).filter { it.fields.isNotEmpty() }
    else
        throw IllegalArgumentException("Invalid type $source. Must call canConvert.")

    override fun canConvert(source: Any): Boolean = source is SdmResourceUpdateEvent

    private suspend fun convert(source: SdmResourceUpdateEvent): InfluxMeasurement {
        val deviceId = source.resourceUpdate.name.getDeviceId()
        val device = deviceDao.device(deviceId)
        val location = device.location(structureDao, roomDao)

        return measurement("temperature") {
            time(source.timestamp)

            addDeviceTags(device, location)
            addFields(source.resourceUpdate.traits.values)
        }
    }
}

data class DeviceLocation(val structure: Structure, val room: Room)

suspend fun Device.location(structureDao: StructureDao, roomDao: RoomDao) = coroutineScope {
    val structure = async { structureDao.structure(structureId) }
    val room = async { roomDao.room(structureId, roomId) }
    DeviceLocation(structure.await(), room.await())
}

fun InfluxMeasurementBuilder.addDeviceTags(
    device: Device,
    location: DeviceLocation
) {
    with(device) {
        tag("device_id") to id.id
        tag("device_custom_name") to customName
    }
    with(location.structure) {
        tag("structure_id") to id.id
        tag("structure_name") to customName
    }
    with(location.room) {
        tag("room_id") to id.id
        tag("location") to customName
        tag("device_name") to "$customName (${device.customName})"
    }
}

fun InfluxMeasurementBuilder.addFields(traits: Iterable<Trait>) = traits.forEach { addFields(it) }

fun InfluxMeasurementBuilder.addFields(trait: Trait) {
    when (trait) {
        is DeviceTrait -> addFields(trait)
    }
}

fun InfluxMeasurementBuilder.addFields(trait: DeviceTrait) {
    when (trait) {
        is HumidityTrait -> addAttributes(trait)
        is TemperatureTrait -> addAttributes(trait)
        is ThermostatEcoTrait -> addAttributes(trait)
        is ThermostatHvacTrait -> addAttributes(trait)
        is ThermostatModeTrait -> addAttributes(trait)
        is ThermostatTemperatureSetpointTrait -> addAttributes(trait)
        is ConnectivityTrait -> addAttributes(trait)
    }
}

fun InfluxMeasurementBuilder.addAttributes(trait: HumidityTrait) {
    field("humidity") to trait.ambientHumidityPercent
}

fun InfluxMeasurementBuilder.addAttributes(trait: TemperatureTrait) {
    field("ambient_temperature") to trait.ambientTemperatureCelsius
}

fun InfluxMeasurementBuilder.addAttributes(trait: ThermostatEcoTrait) {
    field("eco_temperature_high") to trait.coolCelsius
    field("eco_temperature_low") to trait.heatCelsius
    field("eco_temperature_mode") to trait.mode.toLowerCase()
}

fun InfluxMeasurementBuilder.addAttributes(trait: ThermostatHvacTrait) {
    field("hvac_state") to trait.status.toLowerCase()
}

fun InfluxMeasurementBuilder.addAttributes(trait: ThermostatModeTrait) {
    field("hvac_mode") to trait.mode.toLowerCase()
}

fun InfluxMeasurementBuilder.addAttributes(trait: ThermostatTemperatureSetpointTrait) {
    field("target_temperature") to trait.heatCelsius
}

fun InfluxMeasurementBuilder.addAttributes(trait: ConnectivityTrait) {
    field("connection_status") to trait.status
}
