package uk.co.borismorris.nestflux.domain

interface SdmObject {
    val objectId: SdmObjectId
    val id: Id
    val customName: String
    val traits: Map<String, Trait>
}

interface Id {
    val id: String
}

inline class SdmObjectId(val id: String)
