package uk.co.borismorris.nestflux.event

import org.springframework.context.ApplicationEvent
import uk.co.borismorris.nestflux.google.SdmRelationUpdateEvent

class RelationUpdateEvent(relationUpdateEvent: SdmRelationUpdateEvent) : ApplicationEvent(relationUpdateEvent)
