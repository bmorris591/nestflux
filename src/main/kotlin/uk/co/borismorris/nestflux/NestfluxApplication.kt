package uk.co.borismorris.nestflux

import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.getBean
import org.springframework.boot.autoconfigure.info.ProjectInfoAutoConfiguration
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.logging.LogLevel
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.support.BeanDefinitionDsl.Scope.PROTOTYPE
import org.springframework.fu.kofu.application
import org.springframework.fu.kofu.configuration
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.reactive.function.client.WebClient
import uk.co.borismorris.nestflux.config.props.NestProps
import uk.co.borismorris.nestflux.dao.DeviceAccessApiDeviceDao
import uk.co.borismorris.nestflux.dao.DeviceAccessApiRoomDao
import uk.co.borismorris.nestflux.dao.DeviceAccessApiStructureDao
import uk.co.borismorris.nestflux.event.RelationUpdateEvent
import uk.co.borismorris.nestflux.google.GooglePubSubSdmEvents
import uk.co.borismorris.nestflux.google.GoogleSdmDeviceAccess
import uk.co.borismorris.nestflux.influx.webclient.DevicePointConverter
import uk.co.borismorris.nestflux.influx.webclient.SdmEventPointConverter
import uk.co.borismorris.nestflux.logging.LogConfigOnStartup
import uk.co.borismorris.nestflux.logging.LogGitInfoOnStartup
import uk.co.borismorris.nestflux.logging.LogVersionOnStartup
import uk.co.borismorris.nestflux.worker.GatherNestData
import uk.co.borismorris.reactiveflux.conf.InfluxDbProps
import uk.co.borismorris.reactiveflux.webclient.WebclientInfluxClient
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    app.run(args).use {
        try {
            runBlocking(Dispatchers.Default) {
                it.startMonitoring()
            }
        } catch (e: Exception) {
            val logger = LoggerFactory.getLogger("uk.co.borismorris.nestflux.NestfluxApplicationKt")
            logger.error("Error during monitoring", e)
            exitProcess(1)
        }
    }
}

suspend fun ConfigurableApplicationContext.startMonitoring() {
    val runner = getBean<GatherNestData>()
    runner.run()
}

val app = application {
    enable(objectMapperConfig)
    enable(webClientConfig)
    enable(nestClientConfig)
    enable(influxClientConfig)
    enable(startupLoggingConfig)

    beans {
        bean<GatherNestData>()
    }

    logging {
        level = LogLevel.INFO
    }
}

val objectMapperConfig = configuration {
    beans {
        bean { objectMapper() }
    }
}

fun objectMapper() = Jackson2ObjectMapperBuilder.json()
    .findModulesViaServiceLoader(true)
    .build<ObjectMapper>()

val webClientConfig = configuration {
    beans {
        bean(scope = PROTOTYPE) { WebClient.builder() }
    }
}

val nestClientConfig = configuration {
    configurationProperties<NestProps>(prefix = "nest")
    beans {
        bean<DeviceAccessApiStructureDao>()
        bean<DeviceAccessApiDeviceDao>()
        bean<DeviceAccessApiRoomDao>()
        bean<GoogleSdmDeviceAccess>()
        bean<GooglePubSubSdmEvents>()
    }
    listener<RelationUpdateEvent> {
        ref<DeviceAccessApiStructureDao>().refresh()
        ref<DeviceAccessApiDeviceDao>().refresh()
        ref<DeviceAccessApiRoomDao>().refresh()
    }
}

val influxClientConfig = configuration {
    configurationProperties<InfluxDbProps>(prefix = "influxdb")
    beans {
        bean<DevicePointConverter>()
        bean<SdmEventPointConverter>()
        bean<WebclientInfluxClient>()
    }
}

val startupLoggingConfig = configuration {
    beans {
        profile("prod") {
            bean { ProjectInfoAutoConfiguration(ProjectInfoProperties()) }
            bean { ref<ProjectInfoAutoConfiguration>().buildProperties() }
            bean { ref<ProjectInfoAutoConfiguration>().gitProperties() }
        }
        bean<LogConfigOnStartup>()
        bean<LogVersionOnStartup>()
        bean<LogGitInfoOnStartup>()
    }
    listener<ApplicationStartedEvent> {
        ref<LogConfigOnStartup>().onStartup()
        ref<LogVersionOnStartup>().onStartup()
        ref<LogGitInfoOnStartup>().onStartup()
    }
}
