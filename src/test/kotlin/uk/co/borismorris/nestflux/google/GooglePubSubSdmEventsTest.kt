package uk.co.borismorris.nestflux.google

import com.fasterxml.jackson.databind.ObjectMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder

internal class GooglePubSubSdmEventsTest {

    @ParameterizedTest
    @ValueSource(strings = arrayOf("relationUpdated.json", "resourceUpdated.json"))
    fun `A relation updated event deserializes`(source: String) {
        val mapper = Jackson2ObjectMapperBuilder.json()
            .findModulesViaServiceLoader(true)
            .modules(sdmModule)
            .build<ObjectMapper>()
            .let { it.findAndRegisterModules() }

        val sdmEvent = this::class.java.getResourceAsStream(source).use { mapper.readValue(it, SdmEvent::class.java) }

        assertThat(sdmEvent).isNotNull()
    }
}
