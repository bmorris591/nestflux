package uk.co.borismorris.nestflux.domain

import kotlin.reflect.KClass
import kotlin.reflect.full.findAnnotation

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class TraitName(val value: String)

@Target(AnnotationTarget.CONSTRUCTOR)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class TraitConstructor

abstract class Trait {
    open val name: String = this::class.findAnnotation<TraitName>()?.value
        ?: throw IllegalStateException("Traits must be annotated with a name")
}

@TraitName("unknown")
data class UnknownTrait(override val name: String, val data: Map<String, Any>) : Trait()

inline fun <reified T : Trait> parseTraits(traits: Map<String, Any>) = T::class.sealedSubclasses.asSequence()
    .flatMap { sequenceOf(it) + it.sealedSubclasses.asSequence() }
    .flatMap { trait ->
        val name = trait.findAnnotation<TraitName>()
        if (name == null) emptySequence() else sequenceOf(trait to name.value)
    }
    .map { (trait, name) -> name to trait.getTraitConstructor() }
    .filter { (name, _) -> traits.containsKey(name) }
    .associate { (name, ctor) ->
        @Suppress("UNCHECKED_CAST") val properties = traits.getValue(name) as Map<String, Any>
        val trait = ctor.call(properties)
        name to trait
    }

fun <T : Trait> KClass<T>.getTraitConstructor() = constructors.find { it.findAnnotation<TraitConstructor>() != null }
    ?: throw IllegalArgumentException("Trait must have one ctor annotated with @TraitConstructor")
