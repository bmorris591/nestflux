package uk.co.borismorris.nestflux.dao

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect

suspend fun <K, V> Flow<Pair<K, V>>.toMap(): Map<K, V> {
    val map = mutableMapOf<K, V>()
    collect { (k, v) ->
        map[k] = v
    }
    return map
}
